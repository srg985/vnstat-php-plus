<?php

// sidebar labels
$L['summary'] = 'SUMMARY';
$L['hours'] = 'HOURS';
$L['days'] = 'DAYS';
$L['months'] = 'MONTHS';

// main table headers
$L['Summary'] = 'SUMMARY';
$L['Top 10 days'] = 'TOP 10 DAYS';
$L['Last 24 hours'] = 'LAST 24 HOURS';
$L['Last 30 days'] = 'LAST 30 DAYS';
$L['Last 12 months'] = 'LAST 12 MONTHS';

// traffic table columns
$L['In'] = 'IN';
$L['Out'] = 'OUT';
$L['Total'] = 'TOTAL';

// summary rows
$L['This hour'] = 'THIS HOUR';
$L['This day'] = 'THIS DAY';
$L['This month'] = 'THIS MONTH';
$L['All time'] = 'ALL TIME';

// graph text
$L['Traffic data for'] = 'TRAFFIC DATA FOR';
$L['bytes in'] = 'BYTES IN';
$L['bytes out'] = 'BYTES OUT';

// date formats
//$L['datefmt_days'] = '%d %B';
//$L['datefmt_days_img'] = '%d';
//$L['datefmt_months'] = '%B %Y';
//$L['datefmt_months_img'] = '%b';
//$L['datefmt_hours'] = '%l%p';
//$L['datefmt_hours_img'] = '%l';
//$L['datefmt_top'] = '%d %B %Y';

$L['datefmt_days'] = '%d. %B';
$L['datefmt_days_img'] = '%d';
$L['datefmt_months'] = '%B %Y';
$L['datefmt_months_img'] = '%b';
$L['datefmt_hours'] = '%H';
$L['datefmt_hours_img'] = '%H';
$L['datefmt_top'] = '%a %d. %b %Y';
